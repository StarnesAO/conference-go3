from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from django.views.decorators.http import require_http_methods
import json
from events.api_views import ConferenceListEncoder

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference = conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            conference1, conference2 = Conference.objects.filter(id = conference_id )
            content["conference"] = conference1
        except Conference.DoesNotExist:
            return JsonResponse( {"message": "nooooope"}, status = 400)

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder = PresentationDetailEncoder,
            safe = False
            )

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "name"
    ]
class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {"conference": ConferenceListEncoder()}
    def get_extra_data(self,o):
        return {"status": o.status.name}

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder = PresentationDetailEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    else:   ###PUT###
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference, _ = Conference.objects.filter(name=content["conference"]["name"])
                content["name"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "invalid conf"}, status = 400
            )
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
