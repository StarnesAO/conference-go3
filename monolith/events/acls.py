import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
def get_location_pic(city, state):
    print("**********", city, state)
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    response = requests.get(f"https://api.pexels.com/v1/search?query={city},{state}", headers=headers)

    return (response.json()["photos"][0]["url"]) #first photo url

def get_weather(city, state):


    print(city)
    print(state)
    response = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{"US"}&limit=1&appid={OPEN_WEATHER_API_KEY}")
    content = json.loads(response.content)

    lat = content[0]["lat"]
    lon = content[0]["lon"]

    print("lat: ", lat)
    print("lon: ", lon)



    response = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}")
    content = response.json()
    description = content["weather"][0]["main"]
    temp = content["main"]["temp"]
    return {
        "description": description,
        "temp": temp
    }
