from django.http import JsonResponse
import json
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
from .models import Conference, Location, State
from .acls import get_location_pic, get_weather

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location"}, status=400
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder = ConferenceDetailEncoder,
            safe = False
            )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
# openweather map geo to convert city state to lat lon
#http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}&appid={API key}
# openweather weather data
#https://api.openweathermap.org/data/3.0/onecall?lat={lat}&lon={lon}&exclude={part}&appid={API key}

    if request.method == "GET":
        conference = Conference.objects.get(id=id)

        weather = get_weather(conference.location.city, conference.location.state.abbreviation)

        return JsonResponse(
            {"conference": conference,
            "weather": weather},
            encoder = ConferenceDetailEncoder,
            safe = False
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:  ###PUT ###
        content = json.loads(request.body)
        try:
            if "location" in content:
                location, _ = Location.objects.filter(name=content["location"]["name"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "invalid conf number"}, status = 400)

        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            state1, state2 = State.objects.filter(abbreviation=content["state"])
            content["state"]=state2

        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )

        photo = get_location_pic(content["city"], content["state"])

        content["picture_url"] = photo
        location = Location.objects.create(**content)

        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
        #adds new singular location value

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count> 0})
    else: ### PUT ###
        content = json.loads(request.body)
        try:
            if content["state"]:
                state1, state2 = State.objects.filter(abbreviation=content["state"])
                content["state"] = state2
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name"
    ]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

class ConferenceListEncoder(ModelEncoder):  ## takes from Conference Model ###
    model = Conference
    properties = [
        "name"
    ]



class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url"
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation,
        }
